function execute(file, tabId) {
  chrome.tabs.executeScript(tabId, {file:"json2.js"}, function() {
    chrome.tabs.executeScript(tabId, {file:"jquery.js"}, function() {
      chrome.tabs.executeScript(tabId, {file:file});
    });
  });
}

// Called when the user clicks on the browser action.
chrome.browserAction.onClicked.addListener(function(tab) {
    console.log('injecting');
    execute('inject.js');
});

